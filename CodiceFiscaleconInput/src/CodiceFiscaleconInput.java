﻿package codicefiscaleconinput;

import java.util.Scanner;

public class CodiceFiscaleconInput {

    public static void main(String[] args) {
        String codiceCatastale="";
        boolean condition=true;
        int risposta;

        Scanner input = new Scanner(System.in), reader = new Scanner(System.in), readerUno = new Scanner(System.in), inputUno = new Scanner(System.in), inputDue = new Scanner(System.in),readerDue = new Scanner(System.in),inputTre =new Scanner(System.in);
        System.out.print("Inserisci Nome: ");
        String cognome = input.nextLine().toUpperCase();
        System.out.print("Inserisci Cognome: ");
        String nome = input.nextLine().toUpperCase();
        System.out.print("Inserisci il Giorno della data di nascita: ");
        int giornoCasuale = input.nextInt();
        System.out.print("Inserisci il Mese della data di nascita: ");
        String meseCasuale = reader.nextLine();
        System.out.print("Inserisci l'anno della data di nascita: ");
        String annoCasuale = inputUno.nextLine();
        System.out.print("Inserisci sesso (F o M): ");
        String sesso = inputDue.nextLine();
        System.out.print("Vuoi Inserire il codice catastale o il capoluogo di una regione?(1 o 2): ");
        risposta=inputTre.nextInt();
        if(risposta==1){
            System.out.print("Inserisci codice catastale: ");
            codiceCatastale = readerDue.nextLine().toUpperCase();
            condition= true;
        }else if(risposta==2){
            System.out.print("Inserisci Capoluogo: ");
            codiceCatastale = readerDue.nextLine().toUpperCase();
            condition =false;
        }

        int cont = 0;
        int sommaPari = 0, sommaDispari = 0, sommaPariDispari = 0;
        String giornoConvertito="";
        String codiceDiControllo = "";
        String codiceFiscale = "";
        String newCognome = "";
        String newNome = "";

        //Cognome
        //Toglie spazi al cognome
        newCognome += cognome.replace(" ", "").toUpperCase();

        //caso cognome minore di 3 lettere
        if (newCognome.length() < 3) {
            codiceFiscale += newCognome;
            while (codiceFiscale.length() < 3) {
                codiceFiscale += "X";
            }
            cont = 3;
        }

        //caso normale
        for (int i = 0; i < newCognome.length(); i++) {
            if (cont == 3) {
                break;
            }
            if (newCognome.charAt(i) != 'A' && newCognome.charAt(i) != 'E' && newCognome.charAt(i) != 'I' && newCognome.charAt(i) != 'O' && newCognome.charAt(i) != 'U') {
                codiceFiscale += Character.toString(newCognome.charAt(i));
                cont++;
            }
        }

        // nel caso ci siano meno di 3 consonanti
        while (cont < 3) {
            for (int i = 0; i < newCognome.length(); i++) {
                if (cont == 3) {
                    break;
                }
                if (newCognome.charAt(i) == 'A' || newCognome.charAt(i) == 'E' || newCognome.charAt(i) == 'I' || newCognome.charAt(i) == 'O' || newCognome.charAt(i) == 'U') {
                    codiceFiscale += Character.toString(newCognome.charAt(i));
                    cont++;
                }
            }
        }

        //lettere nome 
        //Toglie spazi al nome
        newNome += nome.replace(" ", "");
        cont = 0;

        //caso nome minore di 3 lettere
        if (newNome.length() < 3) {
            codiceFiscale += newNome;
            while (codiceFiscale.length() < 6) {
                codiceFiscale += "X";
            }
            cont = 3;
        }
        int x = cont;

        //caso normale
        for (int i = 0; i < newNome.length(); i++) {
            if (newNome.charAt(i) != 'A' && newNome.charAt(i) != 'E' && newNome.charAt(i) != 'I' && newNome.charAt(i) != 'O' && newNome.charAt(i) != 'U') {
                cont++;
            }
        }
        //blocco per più di 3 consonanti

        if (cont > 3) {
            int j = 0;
            boolean condizione = true;
            for (int i = 0; i < newNome.length(); i++) {
                condizione = true;
                if (newNome.charAt(i) != 'A' && newNome.charAt(i) != 'E' && newNome.charAt(i) != 'I' && newNome.charAt(i) != 'O' && newNome.charAt(i) != 'U') {
                    j++;
                } else {
                    condizione = !condizione;
                }
                if ((j == 1 || j == 3 || j == 4) && condizione) {
                    codiceFiscale += newNome.charAt(i);
                }
            }
            cont = 3;
        } else {
            cont = x;
        }

        //Caso normale
        for (int i = 0; i < newNome.length(); i++) {
            if (cont == 3) {
                break;
            }
            if (newNome.charAt(i) != 'A' && newNome.charAt(i) != 'E' && newNome.charAt(i) != 'I' && newNome.charAt(i) != 'O' && newNome.charAt(i) != 'U') {
                codiceFiscale += Character.toString(newNome.charAt(i));
                cont++;
            }
        }

        // nel caso ci siano meno di 3 consonanti
        while (cont < 3) {
            for (int i = 0; i < newNome.length(); i++) {
                if (cont == 3) {
                    break;
                }
                if (newNome.charAt(i) == 'A' || newNome.charAt(i) == 'E' || newNome.charAt(i) == 'I' || newNome.charAt(i) == 'O' || newNome.charAt(i) == 'U') {
                    codiceFiscale += Character.toString(newNome.charAt(i));
                    cont++;
                }
            }
        }

        //Per anni
        codiceFiscale += annoCasuale.substring(2, 4);

        //Calcolo del mese
        switch (meseCasuale) {
            case "1":
                codiceFiscale += "A";
                break;
            case "01":
                codiceFiscale += "A";
                break;
            case "2":
                codiceFiscale += "B";
                break;
            case "02":
                codiceFiscale += "B";
                break;
            case "3":
                codiceFiscale += "C";
                break;
            case "03":
                codiceFiscale += "C";
                break;
            case "4":
                codiceFiscale += "D";
                break;
            case "04":
                codiceFiscale += "D";
                break;
            case "5":
                codiceFiscale += "E";
                break;
            case "05":
                codiceFiscale += "E";
                break;
            case "6":
                codiceFiscale += "H";
                break;
            case "06":
                codiceFiscale += "H";
                break;
            case "7":
                codiceFiscale += "L";
                break;
            case "07":
                codiceFiscale += "L";
                break;
            case "8":
                codiceFiscale += "M";
                break;
            case "08":
                codiceFiscale += "M";
                break;
            case "9":
                codiceFiscale += "P";
                break;
            case "09":
                codiceFiscale += "P";
                break;
            case "10":
                codiceFiscale += "R";
                break;
            case "11":
                codiceFiscale += "S";
                break;
            case "12":
                codiceFiscale += "T";
                break;
        }

        //Calcolo del giorno
        if (sesso.charAt(0) == 'F' || sesso.charAt(0) == 'f') {
            codiceFiscale += giornoCasuale + 40;
        } else {
            if (giornoCasuale < 10) {
                //giornoConvertito=Integer.toString(giornoCasuale).replace("0", "");
                codiceFiscale += 0;
                codiceFiscale += giornoCasuale;
            } else {
                codiceFiscale += giornoCasuale;
            }
        }

        //Inserimento del codice catastale
        
        if(condition==false){
        switch(codiceCatastale){
            case "ANCONA":
                codiceFiscale+="A271";
                break;
            case "L'AQUILA":
                codiceFiscale+="A345";
                break;
            case "BOLOGNA":
                codiceFiscale+="A944";
                break;
            case "CAMPOBASSO":
                codiceFiscale+="B519";
                break;
            case "FIRENZE":
                codiceFiscale+="D612";
                break;
            case "MILANO":
                codiceFiscale+="F205";
                break;
            case "PALERMO":
                codiceFiscale+="G273";
                break;
            case "POTENZA":
                codiceFiscale+="G942";
                break;
            case "TORINO":
                codiceFiscale+="L219";
                break;
            case "TRIESTE":
                codiceFiscale+="L424";
                break;
            case "VALLE D'AOSTA":
                codiceFiscale+="A326";
                break;
            case "BARI":
                codiceFiscale+="A662";
                break;
            case "CAGLIARI":
                codiceFiscale+="B354";
                break;
            case "CATANZARO":
                codiceFiscale+="C352";
                break;
            case "GENOVA":
                codiceFiscale+="D969";
                break;
            case "NAPOLI":
                codiceFiscale+="F839";
                break;
            case "PERUGIA":
                codiceFiscale+="F839";
                break;
            case "ROMA":
                codiceFiscale+="H501";
                break;
            case "TRENTO":
                codiceFiscale+="L378";
                break;
            case "VENEZIA":
                codiceFiscale+="L736";
                break;
        }
        }else{
                codiceFiscale += codiceCatastale;
                }
        //calcolo somma caratteri in posizione pari
        for (int i = 1; i <= 13; i = i + 2) {
            switch (codiceFiscale.charAt(i)) {
                case '0':
                    sommaPari += 0;
                    break;
                case '1':
                    sommaPari += 1;
                    break;
                case '2':
                    sommaPari += 2;
                    break;
                case '3':
                    sommaPari += 3;
                    break;
                case '4':
                    sommaPari += 4;
                    break;
                case '5':
                    sommaPari += 5;
                    break;
                case '6':
                    sommaPari += 6;
                    break;
                case '7':
                    sommaPari += 7;
                    break;
                case '8':
                    sommaPari += 8;
                    break;
                case '9':
                    sommaPari += 9;
                    break;
                case 'A':
                    sommaPari += 0;
                    break;
                case 'B':
                    sommaPari += 1;
                    break;
                case 'C':
                    sommaPari += 2;
                    break;
                case 'D':
                    sommaPari += 3;
                    break;
                case 'E':
                    sommaPari += 4;
                    break;
                case 'F':
                    sommaPari += 5;
                    break;
                case 'G':
                    sommaPari += 6;
                    break;
                case 'H':
                    sommaPari += 7;
                    break;
                case 'I':
                    sommaPari += 8;
                    break;
                case 'J':
                    sommaPari += 9;
                    break;
                case 'K':
                    sommaPari += 10;
                    break;
                case 'L':
                    sommaPari += 11;
                    break;
                case 'M':
                    sommaPari += 12;
                    break;
                case 'N':
                    sommaPari += 13;
                    break;
                case 'O':
                    sommaPari += 14;
                    break;
                case 'P':
                    sommaPari += 15;
                    break;
                case 'Q':
                    sommaPari += 16;
                    break;
                case 'R':
                    sommaPari += 17;
                    break;
                case 'S':
                    sommaPari += 18;
                    break;
                case 'T':
                    sommaPari += 19;
                    break;
                case 'U':
                    sommaPari += 20;
                    break;
                case 'V':
                    sommaPari += 21;
                    break;
                case 'W':
                    sommaPari += 22;
                    break;
                case 'X':
                    sommaPari += 23;
                    break;
                case 'Y':
                    sommaPari += 24;
                    break;
                case 'Z':
                    sommaPari += 25;
                    break;
            }
        }

        //calcolo somma caratteri in posizione dispari
        for (int i = 0; i <= 14; i = i + 2) {
            switch (codiceFiscale.charAt(i)) {
                case '0':
                    sommaDispari += 1;
                    break;
                case '1':
                    sommaDispari += 0;
                    break;
                case '2':
                    sommaDispari += 5;
                    break;
                case '3':
                    sommaDispari += 7;
                    break;
                case '4':
                    sommaDispari += 9;
                    break;
                case '5':
                    sommaDispari += 13;
                    break;
                case '6':
                    sommaDispari += 15;
                    break;
                case '7':
                    sommaDispari += 17;
                    break;
                case '8':
                    sommaDispari += 19;
                    break;
                case '9':
                    sommaDispari += 21;
                    break;
                case 'A':
                    sommaDispari += 1;
                    break;
                case 'B':
                    sommaDispari += 0;
                    break;
                case 'C':
                    sommaDispari += 5;
                    break;
                case 'D':
                    sommaDispari += 7;
                    break;
                case 'E':
                    sommaDispari += 9;
                    break;
                case 'F':
                    sommaDispari += 13;
                    break;
                case 'G':
                    sommaDispari += 15;
                    break;
                case 'H':
                    sommaDispari += 17;
                    break;
                case 'I':
                    sommaDispari += 19;
                    break;
                case 'J':
                    sommaDispari += 21;
                    break;
                case 'K':
                    sommaDispari += 2;
                    break;
                case 'L':
                    sommaDispari += 4;
                    break;
                case 'M':
                    sommaDispari += 18;
                    break;
                case 'N':
                    sommaDispari += 20;
                    break;
                case 'O':
                    sommaDispari += 11;
                    break;
                case 'P':
                    sommaDispari += 3;
                    break;
                case 'Q':
                    sommaDispari += 6;
                    break;
                case 'R':
                    sommaDispari += 8;
                    break;
                case 'S':
                    sommaDispari += 12;
                    break;
                case 'T':
                    sommaDispari += 14;
                    break;
                case 'U':
                    sommaDispari += 16;
                    break;
                case 'V':
                    sommaDispari += 10;
                    break;
                case 'W':
                    sommaDispari += 22;
                    break;
                case 'X':
                    sommaDispari += 25;
                    break;
                case 'Y':
                    sommaDispari += 24;
                    break;
                case 'Z':
                    sommaDispari += 23;
                    break;
            }
        }
        sommaPariDispari = (sommaPari + sommaDispari) % 26;

        //Controllo del carattere di controllo
        switch (sommaPariDispari) {
            case 0:
                codiceDiControllo = "A";
                break;
            case 1:
                codiceDiControllo = "B";
                break;
            case 2:
                codiceDiControllo = "C";
                break;
            case 3:
                codiceDiControllo = "D";
                break;
            case 4:
                codiceDiControllo = "E";
                break;
            case 5:
                codiceDiControllo = "F";
                break;
            case 6:
                codiceDiControllo = "G";
                break;
            case 7:
                codiceDiControllo = "H";
                break;
            case 8:
                codiceDiControllo = "I";
                break;
            case 9:
                codiceDiControllo = "J";
                break;
            case 10:
                codiceDiControllo = "K";
                break;
            case 11:
                codiceDiControllo = "L";
                break;
            case 12:
                codiceDiControllo = "M";
                break;
            case 13:
                codiceDiControllo = "N";
                break;
            case 14:
                codiceDiControllo = "O";
                break;
            case 15:
                codiceDiControllo = "P";
                break;
            case 16:
                codiceDiControllo = "Q";
                break;
            case 17:
                codiceDiControllo = "R";
                break;
            case 18:
                codiceDiControllo = "S";
                break;
            case 19:
                codiceDiControllo = "T";
                break;
            case 20:
                codiceDiControllo = "U";
                break;
            case 21:
                codiceDiControllo = "V";
                break;
            case 22:
                codiceDiControllo = "W";
                break;
            case 23:
                codiceDiControllo = "X";
                break;
            case 24:
                codiceDiControllo = "Y";
                break;
            case 25:
                codiceDiControllo = "Z";
                break;
        }

        //Inserimento dell'Ultima cifra ovvero il codice di controllo
        codiceFiscale += codiceDiControllo;

        //Output
        System.out.println(cognome + ";" + nome + ";" + giornoCasuale + "/" + meseCasuale + "/" + annoCasuale + ";" + codiceFiscale.toUpperCase());

    }
        }