
function shuffleArray(o) {
  for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
  return o;
}
function shuffle(array) {
	var myArray = ['0'];
	for (var i = 1; i < array.length; i++){ 
		myArray.push(i);
	}
	newArray = shuffleArray(myArray);
	temporaryArray = new Array();
	for (var i = 0; i < array.length; i++){
		temporaryArray[i] = array[newArray[i]];
	}
	return temporaryArray;
}


function mettiTitoloClip(clipVideo){
    for (var i in vettoreplaylist_titoli) {
        if ((vettoreplaylist_titoli[i].url).replace("%3A",":") == clipVideo){
			$('#titolo_video').html(vettoreplaylist_titoli[i].titolo);
            
        }
    }
}                  
function mettiPosterClip(clipVideo){
    for (var i in vettoreplaylist_titoli) {
        if (vettoreplaylist_titoli[i].url == clipVideo){
            $('.img_poster').attr('src',vettoreplaylist_titoli[i].immagine);
			$('#titolo_video').html(vettoreplaylist_titoli[i].titolo);
        }
    }
} 
function mettiPosterClip_HTML5(clipVideo){
    for (var i in vettoreplaylist_titoli) {
        if (vettoreplaylist_titoli[i].url == clipVideo){
            $("#video").attr("poster",vettoreplaylist_titoli[i].immagine);  
            $("#video").attr("src",unescape(clipVideo.replace("flv","mp4")));
        }
    }
} 
function mute(){
    //console.log("GET PLAYER MUTO 1");
    player.mute();
}
function remove_mute(){
    //console.log("REMOVE PLAYER MUTO 1");
    player.unmute();
}
$(document).ready(function(){
    playlist_go(); 
    top.sonoVisibile = "0";
    var controlMute = "0";
    $('#playlist_video').onScreen({
        container: window,
        direction: 'vertical',
        doIn: function() {
            
            if(top.controlMute == "1" && top.sonoVisibile == "0"){
                controlMute = 0; 
                remove_mute();  
                top.controlMute == "0";
            }
            top.sonoVisibile = "1";
        },
        tolerance: 100,
        throttle: 50,
        toggleClass: 'onScreen',
        lazyAttr: null,
        debug: false
     });
     $("#toggle_video_inside").bind("click",function(e){
        e.preventDefault();
        player.pause();
        $("#widget_video").toggleClass("closed");
        $("#toggle_video_inside").toggleClass("active");
     });
});
var visProp = getHiddenProp();
var clipVisible = true;
if (visProp) {
  var evtname = visProp.replace(/[H|h]idden/, "") + "visibilitychange";
  document.addEventListener(evtname, visChange);
}
function visChange() {
  clipVisible = isHidden() ? !1 : !0;
}
function getHiddenProp() {
  var a = ["webkit", "moz", "ms", "o"];
  if ("hidden" in document) {
    return "hidden";
  }
  for (var b = 0;b < a.length;b++) {
    if (a[b] + "Hidden" in document) {
      return a[b] + "Hidden";
    }
  }
  return null;
}
function isHidden() {
  var a = getHiddenProp();
  return a ? document[a] : !1;
};
var vettoreplaylist = [
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/12/domenicoprimavera_2.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/12/IntervistaDionigiFaccenda.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/inter.18.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.17.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.14.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.13.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.12.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.11.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.10.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.09.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.08.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.071.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/INTERV.03.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/INTERV.04.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/inter.02.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/int.012.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/giorgiosardo.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/FabioSantini2.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/05/avanade_prod.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
{
url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/05/wallfarm_prod.flv'), 
accelerated:true , 
autoPlay: true , scaling:'fit', 
autoBuffering: true 
} , 
];
var vettoreplaylist_titoli = [
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/12/domenicoprimavera_2.flv'), 
titolo : 'Cloud, per OVH è "public" o "dedicated"', 
immagine : 'http://video.html.it/wp-content/uploads/2015/12/domenicoprimavera_2_thumb2.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/12/IntervistaDionigiFaccenda.flv'), 
titolo : 'Brick & Click: Dionigi Faccenda, l\'opportunità IoT e Cloud', 
immagine : 'http://video.html.it/wp-content/uploads/2015/12/IntervistaDionigiFaccenda_thumb4.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/inter.18.flv'), 
titolo : 'Per me il futuro è ora! #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/inter.18_thumb3.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.17.flv'), 
titolo : 'Fa illuminare gli occhi ai ragazzi. #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/interv.17_thumb4.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.14.flv'), 
titolo : 'Aprirsi anche al no profit. #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/interv.14_thumb2.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.13.flv'), 
titolo : 'Aprirsi ad altri sistemi e tecnologie. #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/interv.13_thumb1.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.12.flv'), 
titolo : 'Continuare a integrare altri sistemi. #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/interv.12_thumb1.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.11.flv'), 
titolo : 'Insistere con Visual Studio Code! #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/interv.11_thumb3.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.10.flv'), 
titolo : 'Ottima la metamorfosi. #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/interv.10_thumb1.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.09.flv'), 
titolo : 'Puntare sulla salute! #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/interv.09_thumb4.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.08.flv'), 
titolo : 'Continuare a stupire! #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/interv.08_thumb3.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/interv.071.flv'), 
titolo : 'Realizzare tutto ciò che è iniziato! #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/interv.071_thumb2.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/INTERV.03.flv'), 
titolo : 'Sempre più aperti all\'opensource! #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/INTERV.03_thumb1.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/INTERV.04.flv'), 
titolo : 'Farsi conoscere! #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/INTERV.04_thumb1.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/inter.02.flv'), 
titolo : 'Voglio l\'olografia! #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/inter.02_thumb2.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/int.012.flv'), 
titolo : 'Device e domotica! #dilloalfuturo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/int.012_thumb1.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/giorgiosardo.flv'), 
titolo : '#Future Decoded: Giorgio Sardo', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/giorgiosardo_thumb2.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/11/FabioSantini2.flv'), 
titolo : '#Future Decoded: Fabio Santini', 
immagine : 'http://video.html.it/wp-content/uploads/2015/11/FabioSantini2_thumb2.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/05/avanade_prod.flv'), 
titolo : 'Avanade, le opportunità del 2015 e la nuova unit sarda', 
immagine : 'http://video.html.it/wp-content/uploads/2015/05/avanade_prod_thumb1.jpg' 
}
 ,
{
 url: escape('http://cdn1.video.html.it/wp-content/uploads/2015/05/wallfarm_prod.flv'), 
titolo : 'Wallfarm, l\'orto sospeso per tutti', 
immagine : 'http://video.html.it/wp-content/uploads/2015/05/wallfarm_prod_thumb4.jpg' 
}
 ,
];
vettoreplaylist = shuffle(vettoreplaylist);
    ghvideoLayer = [{
    event:'playerEmbed',
    videoSite:'Html',
    hasPreroll:'yes',
    embedType:'masthead' 
    }];
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','ghvideoLayer','GTM-TS62BW');
    impagina = '<img src="http://video.html.it/bianca.png" class="img_poster"  />';
    impagina += '<img src="http://video.html.it/pulsante_play.png" class="row-common"  />';
    impagina_hidden = '<img style="z-index:999" src="http://video.html.it/bianca.png" class="img_poster"  />';
    impagina_hidden += '<img style="z-index:9999" src="http://video.html.it/pulsante_play.png" class="row-common"  />';
    var player; 
    function playlist_go() {
        if(FlashDetect.installed){
                    var stop_play = 0;
            
                    document.getElementById("playlist_video").innerHTML = "";
                    document.getElementById("playlist_video").onmouseover = "return false";

                    var bin_url = 'http://common.html.it/bin/flowplayer/';
                    var pre_roll_tag = 'http://pubads.g.doubleclick.net/gampad/ads?sz=480x360&iu=/5902/HTML.it/preroll&ciu_szs&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]&nofb=1';
					if (pre_roll_tag != "") {
						var jadv_url_adv;
						if (top.location!=self.location){jadv_url_adv=(document.referrer!='')?document.referrer:document.location.href;}else{jadv_url_adv=document.location.href;}jadv_url_adv=encodeURIComponent(jadv_url_adv);
						pre_roll_tag = pre_roll_tag.replace('[referrer_url]',jadv_url_adv);
						pre_roll_tag = pre_roll_tag.replace('[description_url]',jadv_url_adv);
					}
					var EW_VAST_WRAPPER = escape(pre_roll_tag);	
					
                    sorgentePlayer = bin_url + 'flowplayer.commercial-3.2.15.swf';
                    var titoloClip = $("#titolo_video").html();
                    player = flowplayer
                    (
                            'playlist_video',
                            {
                                    src: sorgentePlayer,						
                                    wmode: "opaque" // This allows the HTML to hide the flash content
                            }
                            , {

                                    key: '#$f2bd0d7935bbdb8a32d',
                                    onLoad: function() {
                                        //console.log("aaa"+top.sonoVisibile);
                                        if(top.sonoVisibile == "0"){
                                            player.mute();
                                            //console.log("MUTO DA PLAY AUTOMATICO");
                                            top.controlMute = "1";
                                        }
                                        ghvideoLayer.push({event:"playerLoaded"});
                                    },

                                    clip: {
                                        autoPlay: false
                                        , scaling:'fit'
                                      , autoBuffering: true
                                      , provider: 'lighttpd'
                                      , onLoad: function(clip) {
                                                     this.setVolume(20);                                     
                                             }
                                      ,onCuepoint: [
                                            [500, 5000, 10000, 15000, 20000, 30000, 40000, 50000, 60000, 90000, 120000, 150000, 180000, 210000, 240000, 270000, 300000],
                                            function(clip, cuepoint) {
                                                    if (cuepoint === 500) {
                                                            for (var i in vettoreplaylist_titoli) {										
                                                                    if (vettoreplaylist_titoli[i].url == escape(clip.url)){
                                                                            titoloClip = vettoreplaylist_titoli[i].titolo;
                                                                    }
                                                            }
                                                            ghvideoLayer.push({event:"videoEvent",eventAction:"Start", clipTitle:titoloClip, videoVisibility:clipVisible});
                                                    } else {
                                                            ghvideoLayer.push({event:"videoCuepoint",clipTitle:titoloClip,clipTime:(cuepoint/1000),videoVisibility:clipVisible});
                                                    }
                                            }
                                            ] 
                                    , onPause: function(clip) {
                                            ghvideoLayer.push({event:"videoEvent",eventAction:"Pausa", clipTitle:titoloClip, videoVisibility:clipVisible});
                                    }
                                    , onResume: function(clip) {
                                            ghvideoLayer.push({event:"videoEvent",eventAction:"Resume",clipTitle:titoloClip});
                                    }
                                     , onStart: function(clip) {
                                            if (stop_play==0) {
                                                stop_play++;
                                                this.stop();
                                                $("#playlist_video").attr("style","visibility:hidden;width:0;overflow:hidden;padding:0");
                                                $("#playlist_video_hidden").show();
                                            }
                                            mettiTitoloClip(clip.url);
                                            EW_VAST_WRAPPER = "";
                                            this.setVolume(20);
                                               
                                      }
                                    }	
                                    , onFinish: function(clip){
                                            ghvideoLayer.push({event:"videoEvent",eventAction:"Fine",clipTitle:titoloClip});
                                    }
                                    , onBufferEmpty: function() {
                                            ghvideoLayer.push({event:"videoEvent",eventAction:"BufferEmpty",clipTitle:titoloClip});
                                    }    									
                                    , play: {
                                            replayLabel: 'Vedi di nuovo'
                                    }
                                    , plugins: {
                                            uif: {
                                              url: bin_url + 'plugins/uif.swf?' + Math.random()
                                              , preroll: EW_VAST_WRAPPER
                                              , ad_countdown: true
                                              , ad_countdown_position: 'top'
                                              , ad_countdown_text: 'Pubblicit&agrave;: il video partir&agrave; tra [time] secondi'
                                              , ad_countdown_text_color: '0xffffff'
                                              , ad_countdown_text_size: '14px'
                                              , ad_countdown_color: '0x000000'
                                              , ad_countdown_opacity: 0.75
                                            }

                                            , lighttpd: {
                                                    url: bin_url + 'plugins/flowplayer.pseudostreaming-3.2.11.swf'
                                              , queryString: escape( 'position=${start}' )
                                            }
                                             
                                    }
                                    , playlist: vettoreplaylist
                            }
                    )
        } else {
            document.getElementById("playlist_video").style.display = "none";	
        }
    }

    
	function playlist_restart(obj){
                setTimeout("$(\"#playlist_video\").attr(\"style\",\"width:99%\");",100);
                // HACK PER RISOLVERE PROBLEMA RENDERING VIDEO
                setTimeout("$(\"#playlist_video\").attr(\"style\",\"width:100%\");",300);
		// FINE HACK
                $(obj).hide();
                player.play();
                player.play();
	}

    
	
	
    contenuto = '<div class="widget-list widget-list-video"><span class="widget-title">HTML.it TV</span><span id="titolo_video" ></span><a href="#" id="toggle_video_inside"></a>';
                                 
    contenuto += ' <div id="widget_video" style="dispay:none"> ';
    contenuto += '<div id="cont_widget_video" > ';
    
    contenuto += ' <div id="playlist_video"></div> ';
    contenuto += ' <div id="playlist_video_hidden" onClick="playlist_restart(this);"></div> ';
    contenuto += ' <div id="video_container"> ';
    contenuto += ' <video id="video" controls="controls" > ';
    contenuto += '  </video>';
    contenuto += ' </div>';    
    contenuto += ' </div>';  
    contenuto += ' </div>';
    $("#video_contenitore").html(contenuto); 

     if(! FlashDetect.installed){

            $("#playlist_video").html("");
            $("#playlist_video").hide();
            $("#playlist_video_hidden").hide();
            mettiPosterClip_HTML5(vettoreplaylist[0].url); 
     }else{
            $("#video_container").html("");
            $("#video_container").hide();
            $("#playlist_video").html(impagina);
            $("#playlist_video_hidden").hide();
	    $("#playlist_video_hidden").html(impagina_hidden);
            mettiPosterClip(vettoreplaylist[0].url);
		
     }
     $("#widget_video").show();
     
<!-- WP Super Cache is installed but broken. The path to wp-cache-phase1.php in wp-content/advanced-cache.php must be fixed! -->