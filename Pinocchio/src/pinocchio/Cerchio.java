/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinocchio;

import zuclib.Tartaruga;

/**
 *
 * @author matteo
 */
public class Cerchio implements FiguraPiana {

    private final Tartaruga t;
    private final double xPos;
    private final double yPos;
    private final double lungRaggio;
    private final double nLati = 360;
    private double area, perimetro;
    private final double sommaAngoliInterni = 360;
    private final double angoli = this.sommaAngoliInterni / this.nLati;
    
    public Cerchio(double x, double y, double r, Tartaruga turtle) {
        this.xPos = x;
        this.yPos = y;
        this.t = turtle;
        this.lungRaggio = r;
        this.perimetro = r * 2 * Math.PI;
        this.area = r * r * Math.PI;
    }
    public double getArea(){
        return this.area;
    }
    public double getPerimetro(){
        return this.perimetro;
    }
    
    @Override
    public void disegna() {
        t.pennaSu();
        t.gotoXY(xPos, yPos);
        t.sinistra(90);
        t.avanti(this.lungRaggio);
        t.destra(90);
        t.pennaGiu();
        for (int i = 0; i < 360; i++) {
            t.avanti((this.lungRaggio /this.sommaAngoliInterni)*Math.PI*2);
            t.destra(this.angoli);
            
        }
    }

    
    
}
