/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinocchio;

import zuclib.Tartaruga;

/**
 *
 * @author matteo
 */
public class Triangolo implements FiguraPiana {

    private final Tartaruga t;
    private final double xPos;
    private final double yPos;
    private final double lungLato;
    private final double nLati = 3;
    private double area, perimetro, altezza;
    private final double sommaAngoliEsterni = 360;
    private final double angoli = this.sommaAngoliEsterni / this.nLati;

    public Triangolo(double x, double y, double l, Tartaruga turtle) {
        this.xPos = x;
        this.yPos = y;
        this.t = turtle;
        this.lungLato = l;
        this.altezza = Math.sqrt((Math.pow(this.lungLato, 2) - Math.pow((this.lungLato / 2), 2)));
        this.perimetro = this.lungLato * 3;
        this.area = (this.altezza * this.lungLato) / 2;
    }

    public double getArea() {
        return this.area;
    }

    public double getPerimetro() {
        return this.perimetro;
    }

    @Override
    public void disegna() {
        t.pennaSu();
        t.gotoXY(xPos, yPos);
        t.sinistra(180);
        t.avanti(this.altezza / 2);
        t.destra(90);
        t.avanti(this.lungLato / 2);
        t.destra(180);
        t.pennaGiu();
        for (int i = 0; i < 3; i++) {
            t.avanti(this.lungLato);
            t.sinistra(this.angoli);
        }
    }

}
