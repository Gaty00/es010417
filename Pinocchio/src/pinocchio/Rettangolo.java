/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinocchio;

import zuclib.Tartaruga;

/**
 *
 * @author matteo
 */
public class Rettangolo implements FiguraPiana {

    private final Tartaruga t;
    private final double xPos;
    private final double yPos;
    ;
    private final double nLati = 4;
    private final double area;
    private final double perimetro;
    private final double base;
    private final double altezza;
    private final double sommaAngoliInterni = 360;
    private final double angoli = this.sommaAngoliInterni / this.nLati;

    public Rettangolo(double x, double y, double b, double a, Tartaruga turtle) {
        this.xPos = x;
        this.yPos = y;
        this.t = turtle;
        this.base = b;
        this.altezza = a;
        this.perimetro = (this.base + this.altezza) * 2;
        this.area = this.base * this.altezza;
    }
    public double getArea(){
        return this.area;
    }
    public double getPerimetro(){
        return this.perimetro;
    }

    public void disegna() {
        t.pennaSu();
        t.gotoXY(xPos, yPos);
        t.sinistra(90);
        t.avanti(this.base / 2);
        t.sinistra(90);
        t.avanti(this.altezza / 2);
        t.sinistra(180);
        t.pennaGiu();
        for (int i = 0; i < 2; i++) {
            t.avanti(this.altezza);
            t.destra(this.angoli);
            t.avanti(this.base);
            t.destra(this.angoli);
        }
    }

}
