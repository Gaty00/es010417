/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinocchio;

import java.util.ArrayList;
import zuclib.*;
import static zuclib.GraficaSemplice.setFinestra;

/**
 *
 * @author matteo
 */
public class Pinocchio {
    public static void main(String[] args) {
        Tartaruga turtle=new Tartaruga();
        setFinestra(1000,900,"ciao");
        
        
        ArrayList<FiguraPiana> array = new ArrayList<>();
        
        array.add(new Cerchio(0.5,0.7,0.1,turtle));
        array.add(new Rettangolo(0.5,0.45,0.2,0.3,turtle));
        array.add(new Triangolo(0.5,0.89,0.2,turtle));
        array.add(new Triangolo(0.62,0.7,0.1,turtle));
        array.add(new Rettangolo(0.45,0.20,0.05,0.2,turtle));
        array.add(new Rettangolo(0.55,0.20,0.05,0.2,turtle));
        
        for (FiguraPiana elemento : array) {
            elemento.disegna();
        }
    }
    
}
