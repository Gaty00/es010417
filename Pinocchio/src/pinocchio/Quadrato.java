/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pinocchio;

import zuclib.Tartaruga;

/**
 *
 * @author matteo
 */
public class Quadrato implements FiguraPiana {

    private final Tartaruga t;
    private final double xPos;
    private final double yPos;
    private final double lungLato;
    private final double nLati = 4;
    private double area, perimetro;
    private final double sommaAngoliInterni = 360;
    private final double angoli = this.sommaAngoliInterni / this.nLati;

    public Quadrato(double x, double y, double l, Tartaruga turtle) {
        this.xPos = x;
        this.yPos = y;
        this.t = turtle;
        this.lungLato = l;
        this.area = Math.pow(this.lungLato, 2);
    }

    public double getArea() {
        return this.area;
    }

    public double getPerimetro() {
        return this.perimetro;
    }
    @Override
    public void disegna() {
        t.pennaSu();
        t.gotoXY(xPos, yPos);
        t.sinistra(90);
        t.avanti(this.lungLato / 2);
        t.sinistra(90);
        t.avanti(this.lungLato / 2);
        t.destra(180);
        t.pennaGiu();
        for (int i = 0; i < 4; i++) {
            t.avanti(this.lungLato);
            t.destra(this.angoli);
        }

    }

    
    

}
